<?php
namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $primaryKey =  ['dni', 'id_reg', 'id_com'];
    public $incrementing = false;
    public $timestamps = false;

    public function commune()
    {
        return $this->belongsTo('App\Commune', 'id_com', 'id_com');
    }

    public function region()
    {
        return $this->belongsTo('App\Region', 'id_reg', 'id_reg');
    }
}
