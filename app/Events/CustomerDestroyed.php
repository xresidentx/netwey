<?php

namespace App\Events;

use App\Customer;

class CustomerDestroyed extends Event
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, array $newAttrs = [])
    {
        $this->customer = $customer;
        $this->newAttrs = $newAttrs;
    }
}
