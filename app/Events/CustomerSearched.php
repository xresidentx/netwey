<?php

namespace App\Events;

use App\Customer;

class CustomerSearched extends Event
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }
}
