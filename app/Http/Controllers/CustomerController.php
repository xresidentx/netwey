<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Events\CustomerDestroyed;
use App\Events\CustomerStored;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CustomerController extends Controller
{
    /**
     * @group  Customer Management
     *
     * Store a new Customer
     * It will return **json** *{'success': true}* if successfully stored otherwise *{'success': false}*
     *
     * **Headers**
     *
     * X-Api-Key: (The api key given by the developer)
     *
     * @bodyParam  dni string required The customer's DNI <br><b>max characters:</b> 45
     * @bodyParam  id_reg integer required The Region ID
     * @bodyParam  id_com integer required The Commune ID
     * @bodyParam  email string required Customer's email address <br><b>max characters:</b> 120
     * @bodyParam  name string required Customer's name <br><b>max characters:</b> 45
     * @bodyParam  last_name string required Customer's last name <br><b>max characters:</b> 45
     * @bodyParam  address string optional Customer's address <br><b>max characters:</b> 255
     * @bodyParam  status string required Customer's address <br><b>enum:</b> A, I, trash  <br> ~**(A)**ctive, **(I)**nactive, trash~
     *
     * @return json
     */
    public function store(Request $request)
    {
        $success = true;

        try {
            // Documento no especificaba si 'date_reg' debia autogenerarse con la fecha actual
            $request->merge(['date_reg' => $request->date_reg ?? \Carbon\Carbon::now() ]);

            // Validate the request data
            $this->validate($request, [
                'dni' => 'required|string|max:45',
                'id_reg' => 'required|integer|exists:regions,id_reg',
                'id_com' => 'required|integer|exists:communes,id_com',
                'email' => 'required|email|max:120',
                'name' => 'required|string|max:45',
                'last_name' => 'required|string|max:45',
                'address' => 'string|max:255',
                'date_reg' => 'required|date',
                'status' => 'string|in:A,I,trash',
            ]);

            // Save the customer
            $customer = new Customer;
            $customer->forceFill([
                'dni' => $request->dni,
                'id_reg' => $request->id_reg,
                'id_com' => $request->id_com,
                'email' => $request->email,
                'name' => $request->name,
                'last_name' => $request->last_name,
                'address' => $request->address,
                'date_reg' => $request->date_reg,
                'status' => $request->status,
            ]);

            $success = $customer->save();

            // Log customer creation
            event(new CustomerStored($customer));
        } catch (ValidationException $e){
            $success = false;
        } catch (Exception $e) {
            $success = false;
        }

        return response()->json([
            'success' => $success
        ]);
    }

    /**
     * @group  Customer Management
     *
     * Delete an existent Customer (Logically)
     * It will return **json** *{'success': true}* if successfully deleted otherwise *{'success': false}*
     *
     * It will return **json** *{'success': false, 'message' => 'Registro no existe'}* if the searched customer not exists
     *
     * @bodyParam  dni string required The customer's DNI <br><b>max characters:</b> 45
     * @bodyParam  id_reg integer required The Region ID
     * @bodyParam  id_com integer required The Commune ID
     *
     * @return json
     */
    public function destroy(Request $request)
    {
        // Documento no especifica que campos pedir para el borrado, por lo que se está tomando la composite primary key [dni, id_reg y id_com]
        // Verify that every attribute of the composite primary key is not empty (in order to unnecessary query the database)
        if (empty($request->dni) || empty($request->id_reg) || empty($request->id_com)) {
            return response()->json([
                    'success' => false,
                    'message' => 'Registro no existe',
                ]);
        }

        // Validate the request data
        try {
            $this->validate($request, [
                'dni' => 'required|string|max:45',
                'id_reg' => 'required|integer', // removed: |exists:communes,id_com  in order to unnecessary query the database
                'id_com' => 'required|integer',
            ]);
        } catch (ValidationException $e){
            return response()->json([
                'success' => false,
            ]);
        }

        // Search the customer by the composite primary key
        $customer = Customer::whereDni($request->dni)
                            ->whereIdReg($request->id_reg)
                            ->whereIdCom($request->id_com)
                            ->whereIn('status', ['A', 'I'])
                            ->first();

        if (is_null($customer)) { // customer not found
            return response()->json([
                    'success' => false,
                    'message' => 'Registro no existe',
                ]);
        }

        // Logically delete (cannot use ->save() cause laravel don't support composite primary key ['dni', 'id_reg', 'id_com'])
        $newAttrs = ['status' => 'trash'];
        $updated = Customer::whereDni($request->dni)
                    ->whereIdReg($request->id_reg)
                    ->whereIdCom($request->id_com)
                    ->update($newAttrs);

        // Log customer destruction
        event(new CustomerDestroyed($customer, $newAttrs));

        return response()->json([
                'success' => (bool) $updated,
            ]);
    }
}
