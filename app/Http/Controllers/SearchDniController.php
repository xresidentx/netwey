<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Events\CustomerSearched;
use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SearchDniController extends Controller
{

    /**
     * @group  Search Customers
     *
     * Search Customer by DNI
     * It will return **json** *{'success': true, 'customer': {...}}* if the customer is found otherwise *{'success': false}*
     *
     * **Headers**
     *
     * X-Api-Key: (The api key given by the developer)
     *
     * @bodyParam  dni string required The customer's DNI <br><b>max characters:</b> 45
     *
     * @return json
     */
    public function __invoke(Request $request)
    {
        if (empty($request->dni)) {
            return response()->json([
                    'success' => false,
                ]);
        }

        // Validate the request data
        try {
            $this->validate($request, [
                'dni' => 'required|string|max:45',
            ]);
        } catch (ValidationException $e) {
            return response()->json([
                'success' => false,
            ]);
        }

        // Do Search
        $customer = Customer::whereDni($request->dni)
                            ->whereStatus('A')
                            ->with(['commune', 'region'])
                            ->first();

        if (is_null($customer)) { // customer not found
            return response()->json([
                'success' => false,
            ]);
        }

        // Log customer search
        event(new CustomerSearched($customer));

        return (new \App\Http\Resources\CustomerResource($customer))
                    ->additional(['success' => true]);
    }
}
