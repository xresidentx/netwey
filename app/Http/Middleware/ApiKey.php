<?php

namespace App\Http\Middleware;

use Closure;

class ApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $api_key = $request->header('X-Api-Key');

        if (empty($api_key) || $api_key !== config('netwey.api_key')) {
            return response()->json('No Autorizado', 401);
        }

        return $next($request);
    }
}
