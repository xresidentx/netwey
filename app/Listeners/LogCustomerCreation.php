<?php

namespace App\Listeners;

use App\Customer;
use App\Events\CustomerStored;
use App\Log;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogCustomerCreation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CustomerStored  $event
     * @return void
     */
    public function handle(CustomerStored $event)
    {
        $log = new Log;
        $log->type = 'store';
        $log->data_old = null;
        $log->data_new = json_encode($event->customer->getAttributes());
        $log->save();
    }
}
