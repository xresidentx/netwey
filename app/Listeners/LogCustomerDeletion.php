<?php

namespace App\Listeners;

use App\Customer;
use App\Events\CustomerDestroyed;
use App\Log;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogCustomerDeletion
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CustomerStored  $event
     * @return void
     */
    public function handle(CustomerDestroyed $event)
    {
        $log = new Log;
        $log->type = 'destroy';
        $log->data_old = json_encode($event->customer->getAttributes());
        $log->data_new = json_encode(array_merge($event->customer->getAttributes(), $event->newAttrs));
        $log->save();
    }
}
