<?php

namespace App\Listeners;

use App\Customer;
use App\Events\CustomerSearched;
use App\Log;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogCustomerSearch
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CustomerStored  $event
     * @return void
     */
    public function handle(CustomerSearched $event)
    {
        $log = new Log;
        $log->type = 'search';
        $log->data_old = null;
        $log->data_new = json_encode($event->customer->getAttributes());
        $log->save();
    }
}
