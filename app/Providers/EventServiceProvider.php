<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\CustomerStored::class => [
            \App\Listeners\LogCustomerCreation::class,
        ],
        \App\Events\CustomerDestroyed::class => [
            \App\Listeners\LogCustomerDeletion::class,
        ],
        \App\Events\CustomerSearched::class => [
            \App\Listeners\LogCustomerSearch::class,
        ],
    ];
}
