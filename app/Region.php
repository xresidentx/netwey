<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $primaryKey = 'id_reg';
    public $timestamps = false;

}
