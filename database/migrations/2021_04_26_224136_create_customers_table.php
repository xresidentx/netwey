<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->string('dni', 45)->comment('Documento de Identidad');
            $table->integer('id_reg')->unsigned();
            $table->integer('id_com')->unsigned();
            $table->string('email', 120)->comment('Correo Electrónico');
            $table->string('name', 45)->comment('Nombre');
            $table->string('last_name', 45)->comment('Apellido');
            $table->string('address', 255)->nullable()->comment('Dirección');
            $table->datetime('date_reg')->comment('Fecha y hora del registro');
            $table->enum('status', ['A', 'I', 'trash'])->default('A')->comment('estado del registro:\nA: Activo\nI : Desactivo\ntrash : Registro eliminado');

            $table->primary(['dni', 'id_reg', 'id_com']);
            $table->index(['id_com', 'id_reg'], 'fk_customers_communes1_idx'); // [OK] Order 'ASC' by default
            $table->unique('email', 'email_unique'); // [OK] Order 'ASC' by default
            $table->foreign('id_reg')->references('id_reg')->on('regions');
            $table->foreign('id_com')->references('id_com')->on('communes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
