---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.

<!-- END_INFO -->
### **Every api route requires the following:**

**Headers**

**X-Api-Key:** KYwCBLbxIIKWkXrkNgCZ1Os3WgX4yhQQ


#Customer Management


<!-- START_089467e7ea475fb2aca445b2d23f6e7d -->
## Store a new Customer
It will return **json** *{&#039;success&#039;: true}* if successfully stored otherwise *{&#039;success&#039;: false}*

**Headers**

X-Api-Key: (The api key given by the developer)

> Example request:

```bash
curl -X POST \
    "netwey.lol/api/customers" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "X-Api-Key: KYwCBLbxIIKWkXrkNgCZ1Os3WgX4yhQQ" \
    -d '{"dni":"quas","id_reg":8,"id_com":10,"email":"vel","name":"blanditiis","last_name":"non","address":"consequatur","status":"sint"}'

```

```javascript
const url = new URL(
    "netwey.lol/api/customers"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-Api-Key": "KYwCBLbxIIKWkXrkNgCZ1Os3WgX4yhQQ",
};

let body = {
    "dni": "quas",
    "id_reg": 8,
    "id_com": 10,
    "email": "vel",
    "name": "blanditiis",
    "last_name": "non",
    "address": "consequatur",
    "status": "sint"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/customers`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `dni` | string |  required  | The customer's DNI <br><b>max characters:</b> 45
        `id_reg` | integer |  required  | The Region ID
        `id_com` | integer |  required  | The Commune ID
        `email` | string |  required  | Customer's email address <br><b>max characters:</b> 120
        `name` | string |  required  | Customer's name <br><b>max characters:</b> 45
        `last_name` | string |  required  | Customer's last name <br><b>max characters:</b> 45
        `address` | string |  optional  | optional Customer's address <br><b>max characters:</b> 255
        `status` | string |  required  | Customer's address <br><b>enum:</b> A, I, trash  <br> ~**(A)**ctive, **(I)**nactive, trash~
    
<!-- END_089467e7ea475fb2aca445b2d23f6e7d -->

<!-- START_a7f46ce8fe6da7da12c003eeab206d97 -->
## Delete an existent Customer (Logically)
It will return **json** *{&#039;success&#039;: true}* if successfully deleted otherwise *{&#039;success&#039;: false}*

It will return **json** *{&#039;success&#039;: false, &#039;message&#039; =&gt; &#039;Registro no existe&#039;}* if the searched customer not exists

> Example request:

```bash
curl -X POST \
    "netwey.lol/api/customer/delete" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "X-Api-Key: KYwCBLbxIIKWkXrkNgCZ1Os3WgX4yhQQ" \
    -d '{"dni":"voluptas","id_reg":6,"id_com":4}'

```

```javascript
const url = new URL(
    "netwey.lol/api/customer/delete"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-Api-Key": "KYwCBLbxIIKWkXrkNgCZ1Os3WgX4yhQQ",
};

let body = {
    "dni": "voluptas",
    "id_reg": 6,
    "id_com": 4
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/customer/delete`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `dni` | string |  required  | The customer's DNI <br><b>max characters:</b> 45
        `id_reg` | integer |  required  | The Region ID
        `id_com` | integer |  required  | The Commune ID
    
<!-- END_a7f46ce8fe6da7da12c003eeab206d97 -->

#Search Customers


<!-- START_ddeb3aad47747ac03ee1875b07575d0c -->
## Search Customer by DNI
It will return **json** *{&#039;success&#039;: true, &#039;customer&#039;: {...}}* if the customer is found otherwise *{&#039;success&#039;: false}*

**Headers**

X-Api-Key: (The api key given by the developer)

> Example request:

```bash
curl -X POST \
    "netwey.lol/api/customer/search_dni" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "X-Api-Key: KYwCBLbxIIKWkXrkNgCZ1Os3WgX4yhQQ" \
    -d '{"dni":"saepe"}'

```

```javascript
const url = new URL(
    "netwey.lol/api/customer/search_dni"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-Api-Key": "KYwCBLbxIIKWkXrkNgCZ1Os3WgX4yhQQ",
};

let body = {
    "dni": "saepe"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/customer/search_dni`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `dni` | string |  required  | The customer's DNI <br><b>max characters:</b> 45
    
<!-- END_ddeb3aad47747ac03ee1875b07575d0c -->

<!-- START_c00bfa3ed87b8a2f34847eb95dfc528d -->
## Search Customer by Email
It will return **json** *{&#039;success&#039;: true, &#039;customer&#039;: {...}}* if the customer is found otherwise *{&#039;success&#039;: false}*

**Headers**

X-Api-Key: (The api key given by the developer)

> Example request:

```bash
curl -X POST \
    "netwey.lol/api/customer/search_email" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "X-Api-Key: KYwCBLbxIIKWkXrkNgCZ1Os3WgX4yhQQ" \
    -d '{"email":"ab"}'

```

```javascript
const url = new URL(
    "netwey.lol/api/customer/search_email"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-Api-Key": "KYwCBLbxIIKWkXrkNgCZ1Os3WgX4yhQQ",
};

let body = {
    "email": "ab"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/customer/search_email`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  required  | The customer's email <br>(i.e. user@example.com) <br><b>max characters:</b> 120
    
<!-- END_c00bfa3ed87b8a2f34847eb95dfc528d -->


