<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//
$router->group(['middleware' => 'api_key'], function () use ($router) {
    $router->post('api/customers', 'CustomerController@store');
    $router->post('api/customer/delete', 'CustomerController@destroy');
    $router->post('api/customer/search_dni', 'SearchDniController');
    $router->post('api/customer/search_email', 'SearchEmailController');
});
