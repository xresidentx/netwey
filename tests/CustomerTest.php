<?php

use App\Commune;
use App\Customer;
use App\Region;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CustomerTest extends TestCase
{
   use DatabaseMigrations;

    /** @test */
    public function cannot_access_routes_without_api_key()
    {
        $response = $this->post('/api/customers');
        $this->assertEquals(401, $this->response->status());

        $response = $this->post('/api/customer/delete');
        $this->assertEquals(401, $this->response->status());
    }

    /** @test */
    public function can_access_routes_with_api_key()
    {
        // function: __authorizedPost() adds API_KEY to the request header
        $response = $this->__authorizedPost('/api/customers');
        $this->assertEquals(200, $this->response->status());

        $response = $this->__authorizedPost('/api/customer/delete');
        $this->assertEquals(200, $this->response->status());
    }

    /** @test */
    public function it_returns_success_true_if_data_is_correct()
    {
        $region = Region::forceCreate(['description' => 'region_description']);
        $commune = Commune::forceCreate(['id_reg' => $region->id_reg, 'description' => 'commune_description']);

        $data = [
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => 'Una dirección',
            'date_reg' => \Carbon\Carbon::now(),
            'status' => 'A',
        ];

        $response = $this->__authorizedPost('/api/customers', $data);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonEquals(['success' => true]);
        $this->seeInDatabase('customers', [ // Assert composite primary key
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com
        ]);
        $this->seeInDatabase('customers', $data); // Assert that the given data was saved on db
    }

    /** @test */
    public function it_returns_success_false_if_data_is_empty()
    {
        $data = [
            //
        ];

        $response = $this->__authorizedPost('/api/customers', $data);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonEquals(['success' => false]);
        $this->assertEquals(0, Customer::count());
    }

    /** @test */
    public function it_returns_success_false_if_dni_is_null()
    {
        $region = Region::forceCreate(['description' => 'region_description']);
        $commune = Commune::forceCreate(['id_reg' => $region->id_reg, 'description' => 'commune_description']);

        $data = [
            'dni' => null,
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => 'Una dirección',
            'date_reg' => \Carbon\Carbon::now(),
            'status' => 'A',
        ];

        $response = $this->__authorizedPost('/api/customers', $data);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonEquals(['success' => false]);
        $this->assertEquals(0, Customer::count());
    }

    /** @test */
    public function it_returns_success_false_if_foreign_keys_not_exist()
    {
        $data = [
            'dni' => '12345678',
            'id_reg' => 9999,
            'id_com' => 9999,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => 'Una dirección',
            'date_reg' => \Carbon\Carbon::now(),
            'status' => 'A',
        ];

        $response = $this->__authorizedPost('/api/customers', $data);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonEquals(['success' => false]);
        $this->assertEquals(0, Customer::count());
    }


    /** @test */
    public function it_returns__Registro_no_existe__if_data_is_empty()
    {
        $data = [
            //
        ];

        $response = $this->__authorizedPost('/api/customer/delete', $data);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonContains(['message' => 'Registro no existe']);
    }

    /** @test */
    public function it_returns__Registro_no_existe__if_an_attribute_of_the_composite_primary_key_is_empty()
    {
        //@@@@ dni
        $response = $this->__authorizedPost('/api/customer/delete', ['dni' => null]);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonContains(['message' => 'Registro no existe']);

        //@@@@ id_reg
        $response = $this->__authorizedPost('/api/customer/delete', ['id_reg' => null]);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonContains(['message' => 'Registro no existe']);

        //@@@@ id_com
        $response = $this->__authorizedPost('/api/customer/delete', ['id_com' => null]);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonContains(['message' => 'Registro no existe']);
    }

    /** @test */
    public function it_returns__registro_no_existe__if_customer_not_exists()
    {
        $data = [
            'dni' => "9999",
            'id_reg' => 999,
            'id_com' => 9999,
        ];

        $response = $this->__authorizedPost('/api/customer/delete', $data);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonContains(['message' => 'Registro no existe']);
    }

    /** @test */
    public function it_returns__success_false__if_validation_fails()
    {
        $data = [
            'dni' => '12345678',
            'id_reg' => "aaa", // validation fail
            'id_com' => 9999,
        ];

        $response = $this->__authorizedPost('/api/customer/delete', $data);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonContains(['success' => false]);
    }

    /** @test */
    public function it_returns__registro_no_existe__if_customer_exists_but_status_is_not_A_or_I()
    {
        $region = Region::forceCreate(['description' => 'region_description']);
        $commune = Commune::forceCreate(['id_reg' => $region->id_reg, 'description' => 'commune_description']);

        $customer = Customer::forceCreate([
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => 'Una dirección',
            'date_reg' => \Carbon\Carbon::now(),
            'status' => 'trash',
        ]);

        $response = $this->__authorizedPost('/api/customer/delete', ['dni' => '12345678', 'id_reg' => $region->id_reg, 'id_com' => $commune->id_com]);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonContains(['message' => 'Registro no existe']);
    }

    /** @test */
    public function it_returns__success_true__if_customer_exists_and_status_is_A_or_I()
    {
        $region = Region::forceCreate(['description' => 'region_description']);
        $commune = Commune::forceCreate(['id_reg' => $region->id_reg, 'description' => 'commune_description']);

        // @@@@@ status = 'A'
        $customer = Customer::forceCreate([
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => 'Una dirección',
            'date_reg' => \Carbon\Carbon::now(),
            'status' => 'A',
        ]);

        $response = $this->__authorizedPost('/api/customer/delete', ['dni' => '12345678', 'id_reg' => $region->id_reg, 'id_com' => $commune->id_com]);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonContains(['success' => true]);
        $this->seeInDatabase('customers', [ // Assert composite primary key
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'status' => 'trash',
        ]);
    }
}
