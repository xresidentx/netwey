<?php

use App\Commune;
use App\Customer;
use App\Region;
use App\Log;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class LogTest extends TestCase
{
   use DatabaseMigrations;

    /** @test */
    public function it_logs_stored_customer()
    {
        $region = Region::forceCreate(['description' => 'region_description']);
        $commune = Commune::forceCreate(['id_reg' => $region->id_reg, 'description' => 'commune_description']);

        $data = [
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => 'Una dirección',
            'date_reg' => null,
            'status' => 'A',
        ];

        $this->__authorizedPost('/api/customers', $data);

        // Asserts
        $row = Log::first();

        $this->assertNull($row->data_old);
        $this->assertNotNull($row->data_new);

        $data_new = json_decode($row->data_new, true);

        $this->assertSame($row->type, 'store');

        $this->assertSame($data_new['dni'], '12345678');
        $this->assertSame($data_new['id_reg'], $region->id_reg);
        $this->assertSame($data_new['name'], 'Un nombre');
    }

    /** @test */
    public function it_logs_destroyed_customer()
    {
        $region = Region::forceCreate(['description' => 'region_description']);
        $commune = Commune::forceCreate(['id_reg' => $region->id_reg, 'description' => 'commune_description']);

        $customer = Customer::forceCreate([
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => 'Una dirección',
            'date_reg' => \Carbon\Carbon::now(),
            'status' => 'A',
        ]);

        $this->__authorizedPost('/api/customer/delete', ['dni' => '12345678', 'id_reg' => $region->id_reg, 'id_com' => $commune->id_com]);

        // Asserts
        $row = Log::first();

        $this->assertNotNull($row->data_old);
        $this->assertNotNull($row->data_new);

        $data_old = json_decode($row->data_old, true);
        $data_new = json_decode($row->data_new, true);

        $this->assertSame($row->type, 'destroy');

        // data_old
        $this->assertSame($data_old['dni'], '12345678');
        $this->assertSame($data_old['id_reg'], $region->id_reg);
        $this->assertSame($data_old['status'], 'A');

        // data_new
        $this->assertSame($data_new['dni'], '12345678');
        $this->assertSame($data_new['id_reg'], $region->id_reg);
        $this->assertSame($data_new['status'], 'trash');
    }

    /** @test */
    public function it_logs_searched_customer_by_email()
    {
        $region = Region::forceCreate(['description' => 'region_description']);
        $commune = Commune::forceCreate(['id_reg' => $region->id_reg, 'description' => 'commune_description']);

        $customer = Customer::forceCreate([
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => 'Una dirección',
            'date_reg' => \Carbon\Carbon::now(),
            'status' => 'A',
        ]);

        $this->__authorizedPost('/api/customer/search_email', ['email' => 'usuario@example.com']);

        // Asserts
        $row = Log::first();

        $this->assertNull($row->data_old);
        $this->assertNotNull($row->data_new);

        $data_new = json_decode($row->data_new, true);

        $this->assertSame('search', $row->type);

        $this->assertSame($data_new['dni'], '12345678');
        $this->assertSame($data_new['email'], 'usuario@example.com');
        $this->assertSame($data_new['id_reg'], $region->id_reg);
        $this->assertSame($data_new['name'], 'Un nombre');
    }

    /** @test */
    public function it_logs_searched_customer_by_dni()
    {
        $region = Region::forceCreate(['description' => 'region_description']);
        $commune = Commune::forceCreate(['id_reg' => $region->id_reg, 'description' => 'commune_description']);

        $customer = Customer::forceCreate([
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => 'Una dirección',
            'date_reg' => \Carbon\Carbon::now(),
            'status' => 'A',
        ]);

        $this->__authorizedPost('/api/customer/search_dni', ['dni' => '12345678']);

        // Asserts
        $row = Log::first();

        $this->assertNull($row->data_old);
        $this->assertNotNull($row->data_new);

        $data_new = json_decode($row->data_new, true);

        $this->assertSame('search', $row->type);

        $this->assertSame($data_new['dni'], '12345678');
        $this->assertSame($data_new['email'], 'usuario@example.com');
        $this->assertSame($data_new['id_reg'], $region->id_reg);
        $this->assertSame($data_new['name'], 'Un nombre');
    }
}

