<?php

use App\Commune;
use App\Customer;
use App\Region;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class SearchDniTest extends TestCase
{
   use DatabaseMigrations;

    /** @test */
    public function cannot_access_routes_without_api_key()
    {
        $response = $this->post('/api/customer/search_dni');
        $this->assertEquals(401, $this->response->status());
    }

    /** @test */
    public function can_access_routes_with_api_key()
    {
        // function: __authorizedPost() adds API_KEY to the request header
        $response = $this->__authorizedPost('/api/customer/search_dni');
        $this->assertEquals(200, $this->response->status());
    }

    /** @test */
    public function it_returns__Registro_no_existe__if_dni_is_empty()
    {
        //@@@@ dni
        $response = $this->__authorizedPost('/api/customer/search_dni', ['dni' => null]);

        // Asserts
        $this->assertEquals(200, $this->response->status());

        $response->seeJsonContains(['success' => false]);
    }

    /** @test */
    public function it_returns__success_false__if_dni_validation_fails()
    {
        $data = [
            'dni' => '123456789123456789123456789123456789123456789123456789123456789',
        ];

        $response = $this->__authorizedPost('/api/customer/search_dni', $data);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonContains(['success' => false]);
    }

    /** @test */
    public function it_returns__success_true__if_dni_is_found_and_status_is_A()
    {
        $region = Region::forceCreate(['description' => 'region_description']);
        $commune = Commune::forceCreate(['id_reg' => $region->id_reg, 'description' => 'commune_description']);

        $customer = Customer::forceCreate([
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => 'Una dirección',
            'date_reg' => \Carbon\Carbon::now(),
            'status' => 'A',
        ]);

        $response = $this->__authorizedPost('/api/customer/search_dni', ['dni' => '12345678']);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonStructure(['customer', 'success']);
        $response->seeJsonEquals([
            'customer' => [
                'name' => 'Un nombre',
                'last_name' => 'Un apellido',
                'address' => 'Una dirección',
                'commune' => 'commune_description',
                'region' => 'region_description',
            ],
            'success' => true,
        ]);
    }

    /** @test */
    public function it_returns__success_true__if_dni_is_found_and_returns_address_null_if_empty()
    {
        $region = Region::forceCreate(['description' => 'region_description']);
        $commune = Commune::forceCreate(['id_reg' => $region->id_reg, 'description' => 'commune_description']);

        $customer = Customer::forceCreate([
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => '',
            'date_reg' => \Carbon\Carbon::now(),
            'status' => 'A',
        ]);

        $response = $this->__authorizedPost('/api/customer/search_dni', ['dni' => '12345678']);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonStructure(['customer', 'success']);
        $response->seeJsonEquals([
            'customer' => [
                'name' => 'Un nombre',
                'last_name' => 'Un apellido',
                'address' => null,
                'commune' => 'commune_description',
                'region' => 'region_description',
            ],
            'success' => true,
        ]);
    }

    /** @test */
    public function it_returns__success_false__if_dni_is_found_and_status_is_I()
    {
        $region = Region::forceCreate(['description' => 'region_description']);
        $commune = Commune::forceCreate(['id_reg' => $region->id_reg, 'description' => 'commune_description']);

        $customer = Customer::forceCreate([
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => 'Una dirección',
            'date_reg' => \Carbon\Carbon::now(),
            'status' => 'i',
        ]);

        $response = $this->__authorizedPost('/api/customer/search_dni', ['dni' => '12345678']);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonStructure(['success']);
        $response->seeJsonEquals([
            'success' => false,
        ]);
    }

    /** @test */
    public function it_returns__success_false__if_dni_is_found_and_status_is_trash()
    {
        $region = Region::forceCreate(['description' => 'region_description']);
        $commune = Commune::forceCreate(['id_reg' => $region->id_reg, 'description' => 'commune_description']);

        $customer = Customer::forceCreate([
            'dni' => '12345678',
            'id_reg' => $region->id_reg,
            'id_com' => $commune->id_com,
            'email' => 'usuario@example.com',
            'name' => 'Un nombre',
            'last_name' => 'Un apellido',
            'address' => 'Una dirección',
            'date_reg' => \Carbon\Carbon::now(),
            'status' => 'trash',
        ]);

        $response = $this->__authorizedPost('/api/customer/search_dni', ['dni' => '12345678']);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonStructure(['success']);
        $response->seeJsonEquals([
            'success' => false,
        ]);
    }

    /** @test */
    public function it_returns__success_false__if_customer_is_not_found()
    {
        $response = $this->__authorizedPost('/api/customer/search_dni', ['dni' => '0000000000']);

        // Asserts
        $this->assertEquals(200, $this->response->status());
        $response->seeJsonStructure(['success']);
        $response->seeJsonEquals([
            'success' => false,
        ]);
    }
}
