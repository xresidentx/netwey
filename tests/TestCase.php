<?php

use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    static $API_KEY;

    public function setUp() : void
    {
        parent::setUp();

        static::$API_KEY = config('netwey.api_key');
    }

    protected function __authorizedPost($uri, array $data = [])
    {
        return $this->post($uri, $data, ['X-API-KEY' => self::$API_KEY]);
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }
}
